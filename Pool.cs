﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Timers;
using BitPool.BitMessage.PyBitMessage;

namespace BitPool
{
    class Pool
    {
        /// <summary>
        /// Provider of BitMessage network connectivity and I/O
        /// </summary>
        private readonly IBitMessageServiceNode _bmServiceNode;

        /// <summary>
        /// Collection of all messages - indexed by msgid (hash in hex form)
        /// </summary>
        private readonly Dictionary<string, DateTime> _items = new Dictionary<string, DateTime>();
        private readonly Timer _timer;

        public Pool(int livenessInterval) {
            _timer = new Timer(livenessInterval * 1000);
            _timer.Elapsed += (sender, args) => {
                foreach (var s in _items.Where(p => p.Value < DateTime.Now).ToList()) {
                    _items.Remove(s.Key);
                    Debug.Print("[{0}]\tRemoved item {1} : Liveness time exceeded.", s.Value);
                }
            };

            _bmServiceNode = new PyBitMessageServiceNode("", "");
            _bmServiceNode.RegisterReceiverHandler(ReceiveHandler);
        }

        /// <summary>
        /// How long a message is kept in the pool before being purged.
        /// </summary>
        public int LiveTime { get; private set; }

        public int Count { get { return _items.Count; } }


        private void ReceiveHandler (object sender, ReceivedMessagesArgs receivedMessagesArgs) { throw new NotImplementedException(); }

    }

    internal class ReceivedMessagesArgs : EventArgs
    {
        public ReceivedMessagesArgs(List<string> ds) { IDs = ds; }
        public List<string> IDs { get; private set; }
    }
}
