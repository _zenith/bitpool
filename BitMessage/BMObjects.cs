﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BitPool.BitMessage
{
    /// <summary>
    /// BitMessage message (BMMessage) at high level for inclusion in pool intermixed with BPMessages.
    /// </summary>
    class BMMessage : IEquatable<PyBitMessage.BitMessage>
    {
        public BMMessage () { TimeCreated = DateTime.UtcNow; }

        public BMMessage (DateTime created) {
            if (created >= DateTime.UtcNow) {
                Log.LogEvent("ERROR - BMMessage constructor - Passed DateTime is in the future. Set to Now instead.");
                TimeCreated = DateTime.UtcNow;
            } else {
                TimeCreated = created;  
            }
        }

        public string ID { get; internal set; } // Msgid

        public byte[] Subject { get; internal set; }
        public string SubjectText {
            get {
                string text;
                try {
                    text = System.Text.Encoding.UTF8.GetString(Data);
                } catch (Exception) {
                    throw new InvalidDataException("Data could not be decoded to a UTF-8 string.");
                }
                return text;
            }
        }

        public byte[] Data { get; internal set; }
        public string DataText
        {
            get {
                string text;
                try {
                    text = System.Text.Encoding.UTF8.GetString(Data);
                } catch (Exception) {
                    throw new InvalidDataException("Data could not be decoded to a UTF-8 string.");
                }
                return text;
            }
        }

        public string TargetAddress { get; internal set; } // Reciever
        public string SourceAddress { get; internal set; } // Sender

        /// <summary>
        /// Time that the message was created, either in a recieve or send context.
        /// </summary>
        public DateTime TimeCreated { get; private set; }

        public MessageEncoding Encoding { get; internal set; }

        public bool Equals (PyBitMessage.BitMessage other) {
            return ID.Equals(other.msgid, StringComparison.Ordinal);
        }
    }

    /// <summary>
    /// BitPool message. Message instance being this type means it originated 
    /// from within BitPool (sending from or redirecting through).
    /// </summary>
    class BPMessage : BMMessage, IEquatable<BMMessage>
    {
        /// <summary>
        /// How much time to wait at minimum before sending/redirecting, where applicable. Zero-span if not-applicable.
        /// </summary>
        public TimeSpan DelayTime { get; internal set; }

        /// <summary>
        /// Message has a requested minimum delay time for sending/redirection.
        /// </summary>
        public bool HasDelay { get { return DelayTime.CompareTo(TimeSpan.Zero) == 0; } }

        public bool Equals (BMMessage other) {
            return ID.Equals(other.ID, StringComparison.Ordinal);
        }
    }

    class BMAddress
    {
        public string Name { get; internal set; } // Label

        /// <summary>
        /// The address itself.
        /// </summary>
        public string Address { get; internal set; }

        public int AddressVersion { get; internal set; }
        public int StreamNumber { get; internal set; }

        public byte[] PublicEncryptionKey { get { throw new NotImplementedException(); } }
        public byte[] PublicSigningKey { get { throw new NotImplementedException(); } }
    }

    enum MessageEncoding : int
    {
        /// <summary>
        /// Any data with this number may be ignored. The sending node might simply be sharing its public key with you.
        /// </summary>
        Ignore = 0,
        /// <summary>
        /// UTF-8. No 'Subject' or 'Body' sections. Useful for simple strings of data, like URIs or magnet links.
        /// </summary>
        Trivial,
        /// <summary>
        /// UTF-8. Uses 'Subject' and 'Body' sections. No MIME is used.
        /// </summary>
        Simple
    }

}
