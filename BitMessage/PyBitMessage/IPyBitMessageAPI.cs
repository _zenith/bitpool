﻿using CookComputing.XmlRpc;

namespace BitPool.BitMessage.PyBitMessage
{
    /// <summary>
    /// XML-RPC API for interop with PyBitMessage
    /// </summary>
    internal interface IPyBitMessageServerAPI : IXmlRpcProxy
    {
        /// <summary>
        /// Gets response 'firstWord-secondWord'. Used as a simple test of the API. 
        /// </summary>
        /// <returns>'firstWord-secondWord'</returns>
        [XmlRpcMethod]
        string helloWorld(string firstWord, string secondWord);

        /// <summary>
        /// Returns the sum of the integers
        /// </summary>
        /// <returns>a + b</returns>
        [XmlRpcMethod]
        int add(int a, int b);

        /// <summary>
        /// Displays a message in PyBitMessage GUI
        /// </summary>
        [XmlRpcMethod]
        void statusBar(string message);

        /// <summary>
        /// Gets the JSON-encoded list of addresses stored in the Address Book
        /// </summary>
        [XmlRpcMethod]
        string listAddresses ();

        /// <summary>
        /// Creates a random address with a random address version and stream version.
        /// </summary>
        /// <param name="label">Distinguishing name for the address.</param>
        /// <returns></returns>
        [XmlRpcMethod]
        string createRandomAddress (string label);

        /// <summary>
        /// Create a random address with a random address version and stream version.
        /// </summary>
        /// <param name="label">Distinguishing name for the address.</param>
        /// <param name="eighteenByteRipe">Use extra computing power to get a shorter address.</param>
        /// <param name="totalDifficulty">Default is 1.</param>
        /// <param name="smallMessageDifficulty">Default is 1.</param>
        [XmlRpcMethod]
        string createRandomAddress (string label, bool eighteenByteRipe, int totalDifficulty, int smallMessageDifficulty);

        /// <summary>
        /// Create an address from a passphrase.
        /// </summary>
        [XmlRpcMethod]
        string createDeterministicAddresses (string passphrase);

        /// <summary>
        /// Create an address from a passphrase.
        /// </summary>
        /// <param name="eighteenByteRipe">Use extra computing power to get a shorter address.</param>
        /// <param name="totalDifficulty">Default is 1.</param>
        /// <param name="smallMessageDifficulty">Default is 1.</param>
        [XmlRpcMethod]
        string createDeterministicAddresses (string passphrase, int numberOfAddresses, int addressVersionNumber, int streamNumber,
                                                    bool eighteenByteRipe, int totalDifficulty, int smallMessageDifficulty);
        /// <summary>
        /// Create an address from a passphrase, but DOES NOT add it to PyBitMessage's Address Book.
        /// </summary>
        /// <param name="passphrase"></param>
        /// <param name="addressVersionNumber"></param>
        /// <param name="streamNumber"></param>
        [XmlRpcMethod]
        string getDeterministicAddress (string passphrase, int addressVersionNumber, int streamNumber);

        /// <summary>
        /// Get JSON-encoded list of all inbox messages
        /// </summary>
        [XmlRpcMethod]
        string getAllInboxMessages();

        /// <summary>
        /// Get a sent message by its message ID (msgid)
        /// </summary>
        /// <param name="msgid">Message ID in hex encoding.</param>
        [XmlRpcMethod]
        string getInboxMessageByID (string msgid);

        /// <summary>
        /// Gets a sent message by its acknowledgement data (ackdata)
        /// </summary>
        /// <param name="ackData">Acknowledgement data in hex encoding.</param>
        [XmlRpcMethod]
        string getSentMessageByAckData (string ackData);

        /// <summary>
        /// Gets JSON-encoded list of all sent messages
        /// </summary>
        [XmlRpcMethod]
        string getAllSentMessages ();

        /// <summary>
        /// Gets a sent message by its message ID (msgid)
        /// </summary>
        /// <param name="msgid">Message ID in hex encoding.</param>
        [XmlRpcMethod]
        string getSentMessageByID (string msgid);

        /// <summary>
        /// Gets JSON-encoded list of sent messages from an address
        /// </summary>
        [XmlRpcMethod]
        string getSentMessagesBySender (string fromAddress);

        /// <summary>
        /// Deletes a message by its message ID (msgid)
        /// </summary>
        /// <param name="msgid">Message ID in hex encoding.</param>
        [XmlRpcMethod]
        void trashMessage(string msgid);

        /// <summary>
        /// Sends a BitMessage
        /// </summary>
        /// <param name="subject">Base64 encoded subject</param>
        /// <param name="message">Base64 encoded message</param>
        /// <returns>Ackdata</returns>
        [XmlRpcMethod]
        string sendMessage (string toAddress, string fromAddress, string subject, string message);

        /// <summary>
        /// Sends a BitMessage
        /// </summary>
        /// <param name="subject">Base64 encoded subject</param>
        /// <param name="message">Base64 encoded message</param>
        /// <param name="encodingType">Type of encoding used for subject and message. Set to 2 if Base64.</param>
        /// <returns></returns>
        [XmlRpcMethod]
        string sendMessage (string toAddress, string fromAddress, string subject, string message, int encodingType);

        /// <summary>
        /// Sends a broadcast in the BitMessage network
        /// </summary>
        /// <param name="fromAddress"></param>
        /// <param name="subject"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        [XmlRpcMethod]
        string sendBroadcast (string fromAddress, string subject, string message);

        /// <summary>
        /// Gets the text displayed in the PyBitMessage GUI
        /// </summary>
        /// <param name="ackdata"></param>
        /// <returns></returns>
        [XmlRpcMethod]
        string getStatus (string ackdata);

        /// <summary>
        /// Adds a subscription to an address.
        /// </summary>
        [XmlRpcMethod]
        void addSubscription (string address);

        /// <summary>
        /// Adds a subscription to an address.
        /// </summary>
        /// <param name="label">Must be Base64 encoded.</param>
        [XmlRpcMethod]
        void addSubscription (string address, string label);

        /// <summary>
        /// Deletes a subscription to an address.
        /// </summary>
        [XmlRpcMethod]
        void deleteSubscription (string addr);

        /// <summary>
        /// Gets the JSON-encoded list of addresses stored in Subscriptions
        /// </summary>
        [XmlRpcMethod]
        string listSubscriptions (); 
    }
}
