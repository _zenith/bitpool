﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace BitPool.BitMessage.PyBitMessage
{
    /// <summary>
    /// Provides access to the BitMessage network through interop with PyBitMessage.
    /// </summary>
    class PyBitMessageServiceNode : IBitMessageServiceNode
    {
        private static readonly byte[] SubjectTag = Encoding.ASCII.GetBytes("BITPOOL::");

        private PyBitMessageClient _client;

        private readonly Timer _pollTimer, _purgeTimer;

        private readonly List<BMMessage> _messages = new List<BMMessage>();


        public event EventHandler<ReceivedMessagesArgs> ReceivedMessages;

        /// <summary>
        /// Start a new instance of a BitMessage node operating through interop with a PyBitMessage instance.
        /// </summary>
        /// <param name="server">Path of the server. If null, default is http://127.0.0.1:1337 </param>
        /// <param name="pollInterval">Interval in seconds between polling PyBitMessage for new inbox messages. Default is 5 minutes.</param>
        /// <param name="purgeInterval">Interval in seconds between purging recieved & sent messages from PyBitMessage. Default is 1 hour.</param>
        public PyBitMessageServiceNode (string username, string password, Uri server = null, int pollInterval = 300, int purgeInterval = 3600) {
            _client = new PyBitMessageClient(username,password, server ?? new Uri("http://127.0.0.1:1337"));
            _pollTimer = new Timer(pollInterval * 1000);
            _pollTimer.Elapsed += PollTimerOnElapsed;
            _pollTimer.AutoReset = true;
            _pollTimer.Start();
            _purgeTimer = new Timer(purgeInterval * 1000);
            _purgeTimer.Elapsed += PurgeTimerOnElapsed;
            _purgeTimer.AutoReset = true;
            _purgeTimer.Start();
        }

        private void PollTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs) {
            var newMessages = _client.GetAllInboxMessages();
            if(newMessages.Count == 0) return; 
            _messages.AddRange(newMessages.Except(_messages));
            ReceivedMessages(this, new ReceivedMessagesArgs(new List<string>()));
        }

        private void PurgeTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs) {

            Log.LogEvent("Purged PyBitMessage.");
        }



        public void Send (byte[] data, string recipient) {
            throw new NotImplementedException();
        }

        public byte[] Receive () {
            throw new NotImplementedException();
        }

        public byte[] Receive (string[] ids) {
            throw new NotImplementedException();
        }

        public byte[] ReceiveAll () {
            throw new NotImplementedException();
        }

        public string GenerateAddress (string label) {
            throw new NotImplementedException();
        }

        public string GenerateAddress (string label, string passphrase) {
            throw new NotImplementedException();
        }

        public void DeleteMessage (string id, bool errorOnMissing) {
            throw new NotImplementedException();
        }

        public void RegisterReceiverHandler (EventHandler<ReceivedMessagesArgs> handler) { ReceivedMessages += handler; }

        public List<byte[]> GetInboxHashes () {
            throw new NotImplementedException();
        }
    }
}
