using System;
using System.Collections.Generic;

namespace BitPool
{
    interface IBitMessageServiceNode
    {
        void Send(byte[] data, string recipient);
        byte[] Receive();
        byte[] Receive (string[] ids);
        byte[] ReceiveAll ();
        string GenerateAddress (string label);
        string GenerateAddress(string label, string passphrase);

        void DeleteMessage(string id, bool errorOnMissing);

        void RegisterReceiverHandler(EventHandler<ReceivedMessagesArgs> handler);

        List<byte[]> GetInboxHashes();


    }
}